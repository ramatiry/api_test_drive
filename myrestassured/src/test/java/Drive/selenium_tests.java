package Drive;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class selenium_tests {
    public WebDriverWait wait;
    public WebDriver driver;
    @BeforeTest
public void Create_a_folder() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\chromedriver.exe");
//        ChromeOptions options = new ChromeOptions();
//        options.addArguments("headless");
        ChromeDriver driver=new ChromeDriver();
//        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://drive-develop.northeurope.cloudapp.azure.com");


        Thread.sleep(1000);
        driver.findElement(By.id("username")).sendKeys("user1");
        driver.findElement(By.id("password")).sendKeys("user1pass");
        driver.findElement(By.id("submit")).click();
        Thread.sleep(1000);

        //While running the tests with UI
//        driver.findElement(By.cssSelector(".nav-wrapper #proceed-button")).click();

    }
    public WebDriver getDriver() {
        return driver;
    }

    public WebDriverWait getWait() {
        return wait;
    }

}

