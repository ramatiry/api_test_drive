package Drive;

import io.restassured.RestAssured;
import static io.restassured.RestAssured.*;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;
import okhttp3.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import page_selenium.Transfer_a_file_to_a_folder;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Base {
    public WebDriverWait wait;
    public WebDriver driver  ;
    String api="/api";
    String link = Transfer_a_file_to_a_folder.link+api;

    public String id;


    String Authorization = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVlNTY4ODMyNDIwM2ZjNDAwNDM1OTFhYSI" +
            "sImFkZnNJZCI6InQyMzQ1ODc4OUBqZWxsby5jb20iLCJnZW5lc2lzSWQiOiI1ZTU2ODgzMjQyMDNmYzQwMDQzNTkxYWEiLCJuYW1lI" +
            "jp7ImZpcnN0TmFtZSI6Iteg15nXmden15kiLCJsYXN0TmFtZSI6IteQ15PXmdeT16EifSwiZGlzcGxheU5hbWUiOiJ0MjM0NTg3ODl" +
            "AamVsbG8uY29tIiwicHJvdmlkZXIiOiJHZW5lc2lzIiwiZW50aXR5VHlwZSI6ImRpZ2ltb24iLCJjdXJyZW50VW5pdCI6Im5pdHJvI" +
            "HVuaXQiLCJkaXNjaGFyZ2VEYXkiOiIyMDIyLTExLTMwVDIyOjAwOjAwLjAwMFoiLCJyYW5rIjoibWVnYSIsImpvYiI6Iteo15XXpte" +
            "XIiwicGhvbmVOdW1iZXJzIjpbIjAyNjY2Njk5OCIsIjA1Mi0xMjM0NTY1Il0sImFkZHJlc3MiOiLXqNeX15XXkSDXlNee157Xqten1" +
            "5nXnSAzNCIsInBob3RvIjpudWxsLCJqdGkiOiJhZTUxYjA0NC02MDFiLTQwZjUtOGE0Mi1iZWZhNDAwMjExM2IiLCJpYXQiOjE2MjE" +
            "5MzU0OTgsImV4cCI6MTYyNDUyNzQ5OCwiZmlyc3ROYW1lIjoi16DXmdeZ16fXmSIsImxhc3ROYW1lIjoi15DXk9eZ15PXoSJ9.NTjH" +
            "ZmnNJDPCVifNIS9dMBrYbEd4LX8Q9PgjLO2Mv30";

    String Cookie = "SignInSecret=V0ymyiYLsOqXmLdPftCZDtPst5pspfZhx7cA441YV3%2Ff2uEYNjW2q92AVJ9M0LxqU4GLFvkQSP0%2Fr" +
            "vuSLfgYbkreGs72qThJX99DLc6h8NUcZ0Cj%2BbnHyvmrLUhl%2FBYGP5XRACXyOrHq%2F5uOSxZ%2BmPYjO2h%2Bs8q61djUimqoF" +
            "6kYCwzQT1B63eOWMZgfY4%2BFt6qv1go3F9fKR6ll5o2zltqs7NWep4xXAYPmVKqr0g9ZIygYjF%2B3FKbvwhgWSUoNs7gxgtYc6Qp" +
            "u6OZwB6f0jNXwtkLKjByyhZ6V77tX%2B4S2RzgC60APV22KD2%2FwSCtNENSdMWIbGyJF1LD4%2Bhss8w%3D%3D; connect.sid=s" +
            "%3AXD1Rpt2ufqC1OcUPDbseKuXFn1VTSPWp.2Vm0htrSqjdmEiAirEchS2Q4lfkOK095aUifqyH%2F2Mk; kd-token=eyJhbGciOi" +
            "JIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVlNTY4ODMyNDIwM2ZjNDAwNDM1OTFhYSIsImFkZnNJZCI6InQyMzQ1ODc4OUBqZWx" +
            "sby5jb20iLCJnZW5lc2lzSWQiOiI1ZTU2ODgzMjQyMDNmYzQwMDQzNTkxYWEiLCJuYW1lIjp7ImZpcnN0TmFtZSI6Iteg15nXmden1" +
            "5kiLCJsYXN0TmFtZSI6IteQ15PXmdeT16EifSwiZGlzcGxheU5hbWUiOiJ0MjM0NTg3ODlAamVsbG8uY29tIiwicHJvdmlkZXIiOiJ" +
            "HZW5lc2lzIiwiZW50aXR5VHlwZSI6ImRpZ2ltb24iLCJjdXJyZW50VW5pdCI6Im5pdHJvIHVuaXQiLCJkaXNjaGFyZ2VEYXkiOiIyM" +
            "DIyLTExLTMwVDIyOjAwOjAwLjAwMFoiLCJyYW5rIjoibWVnYSIsImpvYiI6Iteo15XXpteXIiwicGhvbmVOdW1iZXJzIjpbIjAyNjY" +
            "2Njk5OCIsIjA1Mi0xMjM0NTY1Il0sImFkZHJlc3MiOiLXqNeX15XXkSDXlNee157Xqten15nXnSAzNCIsInBob3RvIjpudWxsLCJqd" +
            "GkiOiJhZTUxYjA0NC02MDFiLTQwZjUtOGE0Mi1iZWZhNDAwMjExM2IiLCJpYXQiOjE2MjE5MzU0OTgsImV4cCI6MTYyNDUyNzQ5OCw" +
            "iZmlyc3ROYW1lIjoi16DXmdeZ16fXmSIsImxhc3ROYW1lIjoi15DXk9eZ15PXoSJ9.NTjHZmnNJDPCVifNIS9dMBrYbEd4LX8Q9Pgj" +
            "LO2Mv30";

    String name;
    List<String> idsAllFiles;
    List<String> nameAllFiles;
    int theNewId;
    RequestSpecification request = RestAssured.given();

}