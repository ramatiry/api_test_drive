package Drive;

import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Get_single_file extends Base {
    @Test
    public void get_single_file(){
        request.header("Authorization",Authorization);
        Response response=request.get(link+"/files/60bc9b28ecd4bb0011fe9f58");
        name=response.body().jsonPath().get("name");
        Assert.assertEquals(name,"569.txt");
        Assert.assertEquals(response.getStatusCode(),200);
        System.out.println(name);
    }
}
