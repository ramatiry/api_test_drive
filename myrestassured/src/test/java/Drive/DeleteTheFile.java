package Drive;

import org.testng.Assert;
import org.testng.annotations.Test;
import io.restassured.response.Response;

public class DeleteTheFile extends Base {

    @Test
    public void DeleteTheFile()  {
        request.header("Content-type","application/jason");
                request.header("Authorization",Authorization)
                .header("Cookie",Cookie);


//        String id="60bc9c9cecd4bb0011fe9f61";
        Response response=request.delete(link+"/files/"+Upload_file.id);
        int statusCode=response.statusCode();
        String JsonString=response.asString();
        System.out.println(JsonString);
        Assert.assertEquals(statusCode,200);
        System.out.println("Status Code is "+statusCode);

    }
}
