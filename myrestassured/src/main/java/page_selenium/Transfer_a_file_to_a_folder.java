package page_selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class Transfer_a_file_to_a_folder extends Page_selenium {
    public Transfer_a_file_to_a_folder(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @FindBy(className = "v-icon notranslate material-icons theme--light white--text")
    protected WebElement button_add_folder_or_files;
    @FindBy(className = "v-btn__content")
    protected List<WebElement> add_folder_or_files;
    @FindBy(id = "input-104")
    protected WebElement Name_The_Folder;
    @FindBy(id = "confirm-button")
    protected WebElement Confirm;
    @FindBy(id = "upload-input")
    protected WebElement upload;
    @FindBy(className = "file-name")
    protected List<WebElement> file_name;
    @FindBy(css = "#move-button .v-btn__content")
    protected WebElement move_button;
    @FindBy(className = "v-list-item__title")
    protected List<WebElement> folders;
    @FindBy(css = "ul~.v-card__actions.popup-confirm #confirm-button")
    protected WebElement accept;

    public static String link = "http://drive-develop.northeurope.cloudapp.azure.com";

//    public Actions actions = new Actions(driver);


   public void transfer() throws InterruptedException {

        Thread.sleep(2000);
        WebElement element=null;

        wait.until(ExpectedConditions.visibilityOf(file_name.get(1)));

        for (int i = 0; i < file_name.size(); i++) {
            if (file_name.get(i).getText().equals("document16")) {
                element = file_name.get(i);
                element.click();
                break;
            }
        }

        Thread.sleep(1500);
        move_button.click();
        WebElement transfer = null;
        Thread.sleep(1500);
        for (int j = 0; j < folders.size() - 1; j++) {
            if (folders.get(j).getText().equals("test1")) {
                Thread.sleep(1500);
                transfer = folders.get(j);
                transfer.click();
            }
            Thread.sleep(1500);
            Thread.sleep(2500);
            accept.click();
            driver.close();
        }
    }
}




